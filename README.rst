============
FastAPI Demo
============

Just a small demo for fastapi

Quickstart
==========

* python3.9 -m venv env
* . .env/bin/activate
* pip -r requirements.txt
* uvicorn fastapidemo.main:app --reload --port 5000
* http://localhost:5000/docs
