from datetime import date
from typing import Optional

from dateutil.relativedelta import relativedelta
from fastapi import FastAPI

from .response import HelloWorld

app = FastAPI(
    title='Fastapi demo API',
    description='Just a demo for fastapi',
)


def get_days_until_birthday(birthday: date, today: date) -> int:
    this_year = (date(today.year, birthday.month, birthday.day) - today).days
    if this_year >= 0:
        return this_year

    next_year = (date(today.year+1, birthday.month, birthday.day) - today).days
    return next_year


@app.get('/hello/', response_model=HelloWorld)
async def hello_world_get(name: str, birthday: Optional[date] = None):
    obj = HelloWorld.construct()
    obj.message = f'Hello {name}!'
    if birthday:
        today = date.today()
        obj.days_until_birthday = get_days_until_birthday(birthday, today)
        if obj.days_until_birthday == 0:
            obj.message += ' Happy birthday! \U0001f973'
        elif obj.days_until_birthday <= 14:
            obj.message += ' Not long until your birthday! \U0000263A'

        obj.age = relativedelta(today, birthday).years

    return obj
