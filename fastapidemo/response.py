import json
from typing import Optional

from pydantic import BaseModel

from .settings import EXAMPLE_PATH  # type: ignore

with (EXAMPLE_PATH / 'hello_world.json').open() as f:
    hello_world_example = json.load(f)


class HelloWorld(BaseModel):
    """
    Hello world
    """

    message: str
    age: Optional[int]
    days_until_birthday: Optional[int]

    class Config:
        title = 'Hello World'
        schema_extra = {
            'example': hello_world_example
        }
